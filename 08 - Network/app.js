const { MongoClient } = require('mongodb')
const chance = require('chance').Chance()
const express = require('express')

const port = process.env.PORT
const url = process.env.MONGO_URL

const db = connect(url)

db.then(db => {
  db.insertMany([
    { name: chance.name(), email: chance.email() },
    { name: chance.name(), email: chance.email() },
    { name: chance.name(), email: chance.email() },
    { name: chance.name(), email: chance.email() },
  ])
})

const app = express()

app.get('/', (req, res) => {
  db.then(db => {
    const items = db.find().toArray((err, items) => {
      if (err) {
        console.error(err)
        res.status(500).end()
      } else {
        res.send(JSON.stringify(items, null, '  '))
      }
    })
  })
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`)
})

function connect(url) {
  return new Promise((resolve, reject) => {
    MongoClient.connect(url, { useNewUrlParser: true }, (err, db) => {
      if (err) {
        reject(err)
      } else {
        resolve(db.db().collection('sample'))
      }
    })
  })
}
