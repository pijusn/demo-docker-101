# Sample 08: Network

```sh
docker network create sample-network

docker build --tag=sample .

docker run -d --network=sample-network --name=database mongo
docker run -d --network=sample-network --publish=4001:3000 --env=MONGO_URL=mongodb://database:27017/sample sample

sleep 1
curl http://127.0.0.1:4001/

docker run -d --network=sample-network --publish=4002:3000 --env=MONGO_URL=mongodb://database:27017/sample sample

sleep 1
curl http://127.0.0.1:4001/ # First container again!
```
