#!/bin/sh

set -ex

docker pull alpine
docker pull mongo
docker pull node:10.11.0

cd 03* && docker build . && cd -
cd 04* && docker build . && cd -
cd 07*/app && docker build . && cd -
cd 07*/base && docker build . && cd -
cd 08* && docker build . && cd -

