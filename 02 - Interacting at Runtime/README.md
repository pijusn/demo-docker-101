# Example 02: Interacting at Runtime

```sh
docker build --tag=sample .
docker run -it sample
```

```sh
docker build --tag=sample .
CID=$(docker run -d sample)
docker exec $CID cat timestamp.txt
```

```sh
docker build --tag=sample .
CID=$(docker run -d sample)
docker exec -it $CID sh
```
