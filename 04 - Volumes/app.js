const fs = require('fs')
const express = require('express')
const chance = require('chance').Chance()

const app = express()
const port = process.env.PORT

app.get('/', (req, res) => {
  fs.readFile('/configs/config.json', (err, data) => {
    if (err) {
      res.status(500).send(`Could not read: ${err}`)
    } else {
      res.send(data)
    }
  })
})

app.get('/v/write', (req, res) => {
  const content = chance.name()
  fs.writeFile('/shared/data.txt', content, err => {
    if (err) {
      res.status(500).send(`Could not write: ${err}`)
    } else {
      res.send({ ok: '👍', message: `I wrote '${content}'` })
    }
  })
})

app.get('/v/read', (req, res) => {
  fs.readFile('/shared/data.txt', (err, data) => {
    if (err) {
      res.status(500).send(`Could not read: ${err}`)
    } else {
      res.send({ content: data.toString('utf8') })
    }
  })
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`)
})
