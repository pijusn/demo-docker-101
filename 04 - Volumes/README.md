# Sample 04: Volumes

```sh
docker build --tag=sample .
docker run -d --volume="$(pwd)/configs/A:/configs" --publish=3100:3000 sample
docker run -d --volume="$(pwd)/configs/B:/configs" --publish=3200:3000 sample

curl http://127.0.0.1:3100/
curl http://127.0.0.1:3200/
```

```sh
docker build --tag=sample .
docker volume create --name=samplestore
docker run -d --volume="samplestore:/shared" --publish=3100:3000 sample
docker run -d --volume="samplestore:/shared" --publish=3200:3000 sample

curl http://127.0.0.1:3100/v/write
curl http://127.0.0.1:3200/v/read
```
