# Sample 07: Onbuild

```sh
cd base
docker build --tag=sample:onbuild .
cd ..

cd app
docker build --tag=sample:app .
cd ..

docker run -d sample:app
```
