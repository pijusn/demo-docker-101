# Example 00: Hello World

```sh
docker build --tag=sample .
docker run -it sample
```

Hint: it's supposed to fail.
