# Sample 05: Circular Images

```sh
docker build --tag=sample .
docker history sample
# Now you are expected to change Dockerfile with `FROM sample`

docker build --tag=sample .
docker build --tag=sample .
docker build --tag=sample .
docker build --tag=sample .
docker history sample
# ...
```
