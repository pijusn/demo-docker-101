# Sample 03: Node Express App

```sh
docker build --tag=sample .
docker run -d --publish=3000 sample
docker ps # Look up the port.
# curl http://127.0.0.1:32000/
```

```sh
docker build --tag=sample .
docker run -d --publish=3000:3000 sample
docker ps
curl http://127.0.0.1:3000/
```
