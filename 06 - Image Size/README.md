# Sample 06: Image Size

```sh
docker build --file=1.Dockerfile --tag=sample .
docker images | grep sample
docker history sample

docker build --file=2.Dockerfile --tag=sample .
docker images | grep sample
docker history sample

docker build --file=1.Dockerfile --tag=sample --squash .
docker images | grep sample
docker history sample
```
