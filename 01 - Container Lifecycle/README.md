# Example 01: Container Lifecycle

```sh
docker build --tag=sample .
docker ps --all

CID=$(docker create sample)
echo $CID

docker start $CID
docker ps --all
docker stop $CID
docker ps --all

docker start $CID
docker stop $CID

docker logs $CID
```
