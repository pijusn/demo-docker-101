#!/bin/sh

echo "I just started!"

stop_everything() {
  echo "Interrupted. Exiting."
  exit 0
}

trap "stop_everything" SIGINT
trap "stop_everything" SIGTERM

while :
do
	date
	sleep 1
done

echo "I finished!"
